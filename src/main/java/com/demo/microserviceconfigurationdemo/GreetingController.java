//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.demo.microserviceconfigurationdemo;

import java.util.List;
import java.util.Map;

import com.demo.microserviceconfigurationdemo.DbConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    @Value("some static string")
    private String staticmessage;
    @Value("${my.greeting : defailt value is here }")
    private String greetingmessage;
    @Value("${my.list.values}")
    private List<String> listvalues;
    @Value("#{${dbValues}}")
    private Map<String, String> dbValues;
    @Autowired
    private DbConfig dbConfig;
    @Autowired
    private Environment env;

    public GreetingController() {
    }

    @GetMapping({"/greeting"})
    public String greeting() {
        return this.dbConfig.getConnection() + this.dbConfig.getHost();
    }

    @GetMapping({"/envdetails"})
    private String getEnvironmentDetails() {
        return this.env.toString();
    }
}
